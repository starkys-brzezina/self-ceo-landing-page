import styled from 'styled-components';
import Section from './Section';
import { Grid, Row, Col } from 'react-styled-flexboxgrid';
import { SC } from '../styles/theme';
import { withTranslation, WithTranslation } from '../app/i18n';
import * as Yup from 'yup';
import Form from './Form';
import Input from './Input';
import Select from './Select';
import TextArea from './TextArea';
import { Button } from './Button';
import { Formik } from 'formik';
import { useState } from 'react';

interface ContactTemplateProps extends WithTranslation {}

const encode = (data: any) => {
  return Object.keys(data)
    .map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&');
};

const ContactTemplate: SC<ContactTemplateProps> = ({ className, t }) => {
  const [sent, setSent] = useState(false);

  const onSubmit = (values: any) => {
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({ 'form-name': 'contact', ...values, topic: values.topic.value }),
    })
      .then(() => {
        setSent(true);
      })
      .catch((error) => alert(error));
  };

  const initialValues = {};
  const validationSchema = Yup.object().shape({
    fullName: Yup.string().required(t('validation.required', { field: 'fullName' })),
    email: Yup.string()
      .email('validation.email')
      .required(t('validation.required', { field: 'email' })),
    topic: Yup.object().required(t('validation.required', { field: 'topic' })),
    message: Yup.string().required(t('validation.required', { field: 'message' })),
  });

  return (
    <Section className={className} id="contact">
      <Grid>
        <Row>
          <Col xs={12}>
            <h2 className="title">{t('contact.title')}</h2>
          </Col>
        </Row>
        <Row>
          <Col xs={12} md={6}>
            <div className="image" />
          </Col>
          <Col xs={12} md={6}>
            {sent ? (
              <div className="successfully-sent">
                <h4>{t('contact.success')}</h4>
              </div>
            ) : (
              <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                <Form method="POST" name="contact" data-netlify="true" action="contact">
                  <input type="hidden" name="form-name" value="contact" />
                  <Input name="fullName" placeholder={t('contact.fields.name')} />
                  <Input name="email" placeholder={t('contact.fields.email')} />
                  <Select
                    name="topic"
                    options={[
                      { label: t('contact.fields.options.bug-report'), value: t('contact.fields.options.bug-report') },
                      { label: t('contact.fields.options.other'), value: t('contact.fields.options.other') },
                    ]}
                    placeholder={t('contact.fields.topic')}
                  />
                  <TextArea name="message" placeholder={t('contact.fields.message')} />
                  <Button>Send</Button>
                </Form>
              </Formik>
            )}
          </Col>
        </Row>
      </Grid>
    </Section>
  );
};

const Contact = styled(ContactTemplate)`
  .title {
    text-align: center;
    margin-bottom: 5rem;
  }
  .image {
    width: 100%;
    height: 90vw;
    background: url('/images/contact.png');
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center top;

    @media (min-width: ${({ theme }) => theme.flexboxgrid.breakpoints.sm}rem) {
      width: 420px;
      height: 440px;
      margin: 0 auto;
    }

    @media (min-width: ${({ theme }) => theme.flexboxgrid.breakpoints.lg}rem) {
      width: 500px;
      margin-top: 15px;
    }
  }

  button {
    max-width: 100% !important;
  }

  .successfully-sent {
    display: flex;
    justify-content: center;
    height: 100%;
    align-items: center;
  }
`;

export default withTranslation('common')(Contact);
