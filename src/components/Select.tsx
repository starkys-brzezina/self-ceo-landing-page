import { Field, FieldAttributes, FieldProps } from 'formik';
import styled, { SC } from '../styles/theme';
import ReactSelect, { ValueType } from 'react-select';
import { Option } from 'react-select/src/filters';
import classnames from 'classnames';

interface SelectProps extends FieldAttributes<any> {
  options: Option[];
}

const SelectTemplate: SC<SelectProps> = (props) => {
  return (
    <Field
      component={({ field, form }: FieldProps) => {
        const onChange = (value: ValueType<Option>) => {
          form.setFieldValue(field.name, value);
        };

        const onTouched = () => {
          form.setFieldTouched(field.name, true);
        };

        return (
          <div className={props.className}>
            <ReactSelect
              {...props}
              classNamePrefix="select"
              className={classnames({
                error: (form.touched[field.name] || form.submitCount > 0) && form.errors[field.name],
              })}
              value={field.value}
              onChange={onChange}
              onMenuOpen={onTouched}
            />
            {(form.touched[field.name] || form.submitCount > 0) && form.errors[field.name] && (
              <span className="error_message">{form.errors[field.name]}</span>
            )}
          </div>
        );
      }}
      {...props}
    />
  );
};

const Select = styled(SelectTemplate)`
  .select {
    &__control {
      display: flex;
      background: #ffffff;
      /* Light Black */

      border: 1px solid #7a7580;
      box-sizing: border-box;
      border-radius: 7px;
      padding: 0.2rem 1rem 0.2rem;
      margin: 1rem 0;
      width: 100%;
      font-size: ${({ theme }) => theme.fontSizes.textBase};
      line-height: ${({ theme }) => theme.lineHeights.textBase};

      &--is-focused {
        border-color: ${({ theme }) => theme.colors.primary} !important;
        box-shadow: 0 0 1px ${({ theme }) => theme.colors.primary};
      }
    }
    &__option {
      &--is-focused {
        background: rgba(98, 49, 158, 0.2) !important;
      }
      &:hover {
        background: rgba(98, 49, 158, 0.1.5) !important;
      }
      &--is-selected {
        background: rgba(98, 49, 158, 1) !important;
        color: white;

        &:hover {
          background: rgba(98, 49, 158, 0.9) !important;
        }
      }
    }
  }

  .error .select__control {
    border-color: ${({ theme }) => theme.colors.danger};
    box-shadow: 0 0 1px ${({ theme }) => theme.colors.danger};
  }

  .error_message {
    color: ${({ theme }) => theme.colors.danger};
  }
`;

export default Select;
