import { Field, FieldAttributes, FieldProps } from 'formik';
import styled, { SC } from '../styles/theme';
import classnames from 'classnames';

interface InputProps extends FieldAttributes<any> {}

const InputTemplate: SC<InputProps> = (props) => {
  return (
    <Field
      {...props}
      render={({ field, form }: FieldProps) => {
        return (
          <div className={props.className}>
            <input
              {...props}
              {...field}
              className={classnames('input', {
                error: (form.touched[field.name] || form.submitCount > 0) && form.errors[field.name],
              })}
            />
            {(form.touched[field.name] || form.submitCount > 0) && form.errors[field.name] && (
              <span className="error_message">{form.errors[field.name]}</span>
            )}
          </div>
        );
      }}
    />
  );
};

const Input = styled(InputTemplate)`
  .input {
    display: flex;
    background: #ffffff;
    /* Light Black */

    border: 1px solid #7a7580;
    box-sizing: border-box;
    border-radius: 7px;
    padding: 0.6rem 1rem 0.5rem;
    margin: 1rem 0;
    width: 100%;
    font-size: ${({ theme }) => theme.fontSizes.textBase};
    line-height: ${({ theme }) => theme.lineHeights.textBase};

    &:focus {
      border-color: ${({ theme }) => theme.colors.primary};
      box-shadow: 0 0 1px ${({ theme }) => theme.colors.primary};
    }

    &.error {
      border-color: ${({ theme }) => theme.colors.danger};
      box-shadow: 0 0 1px ${({ theme }) => theme.colors.danger};
    }
  }

  .error_message {
    color: ${({ theme }) => theme.colors.danger};
  }
`;

export default Input;
