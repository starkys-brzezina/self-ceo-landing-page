import styled from 'styled-components';
import { Form as FormikForm } from 'formik';
import { SC } from '../styles/theme';
import * as React from 'react';

const FormTemplate: SC<React.FormHTMLAttributes<HTMLFormElement>> = (props) => {
  return <FormikForm {...(props as any)}>{props.children}</FormikForm>;
};

const Form = styled(FormTemplate)``;

export default Form;
