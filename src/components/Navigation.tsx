import styled from 'styled-components';
import { Grid, Col, Row } from 'react-styled-flexboxgrid';
import { SC } from '../styles/theme';
import { withTranslation, WithTranslation } from '../app/i18n';
import { rem } from 'polished';
import { useState } from 'react';
import { LinkButton } from './Button';
import useMobileDetect from '../app/hooks/useMobileDetect';

interface NavigationTemplateProps extends WithTranslation {}

const NavigationTemplate: SC<NavigationTemplateProps> = ({ className, t }) => {
  const [open, setOpen] = useState(false);
  const toggleOpen = () => setOpen(!open);
  const currentDevice = useMobileDetect();

  return (
    <nav className={className}>
      <Grid>
        <Row>
          <Col xs={12} md={6}>
            <div className="navbar-header">
              <a className="navbar-brand" href="/">
                <div className="navbar-logo"></div>
                <h1>{t('navigation.brand')}</h1>
              </a>
              <button className={`navbar-toggle${open ? ' open' : ''}`} onClick={toggleOpen}>
                <img src="/images/menu.svg" />
              </button>
            </div>
          </Col>
          <Col xs={12} md={6}>
            <ul className={`navbar-nav${open ? ' open' : ''}`}>
              <li>
                <a href="/#top">{t('navigation.home')}</a>
              </li>
              <li>
                <a href="/#features">{t('navigation.features')}</a>
              </li>
              <li>
                <a href="/#contact">{t('navigation.contact')}</a>
              </li>
              <li>
                <LinkButton
                  className="download"
                  secondary
                  href={
                    currentDevice.isAndroid() ? process.env.NEXT_PUBLIC_GOOGLE_PLAY : process.env.NEXT_PUBLIC_APP_STORE
                  }
                >
                  {t('navigation.download')}
                </LinkButton>
              </li>
            </ul>
          </Col>
        </Row>
      </Grid>
    </nav>
  );
};

const Navigation = styled(NavigationTemplate)`
  padding: ${rem('20px')} 0;

  .download {
    line-height: 16px;
    position: relative;
    top: 13px;
    display: none;

    @media (min-width: ${({ theme }) => theme.flexboxgrid.breakpoints.md}rem) {
      display: block;
    }
  }

  .navbar-header {
    display: flex;
    padding: ${rem('8px')} 0;

    .navbar-logo {
      width: 44px;
      height: 44px;
      margin-right: 0.7rem;
      background-image: url('/images/AppIconSmall.png');
    }

    .navbar-brand {
      color: ${({ theme }) => theme.colors.default};
      display: flex;
      align-items: center;

      &:hover,
      &:focus {
        text-decoration: none;
      }

      h1 {
        font-size: 2rem;
        line-height: 3.1rem;
        font-family: ${({ theme }) => theme.fonts.fontHeadings};

        @media (min-width: ${({ theme }) => theme.flexboxgrid.breakpoints.md}rem) {
          font-size: 1.7rem;
        }
      }
    }

    .navbar-toggle {
      background: transparent;
      border: none;
      margin-left: auto;

      @media (min-width: ${({ theme }) => theme.flexboxgrid.breakpoints.md}rem) {
        display: none;
      }
    }
  }
  .navbar-nav {
    display: none;
    justify-content: flex-end;
    list-style-type: none;
    flex-direction: column;
    padding-left: 0;
    align-items: center;
    position: absolute;
    left: 0;
    right: 0;
    background: #fff;
    z-index: 9999;
    padding: 0 0 3rem;

    &.open {
      display: flex;
    }

    @media (min-width: ${({ theme }) => theme.flexboxgrid.breakpoints.md}rem) {
      display: flex;
      flex-direction: row;
      padding: 0;
      margin: 0;
      position: initial;
    }

    li a:not(.download),
    li a:not(.download):active,
    li a:not(.download):visited {
      font-family: Jost;
      font-style: normal;
      font-weight: normal;
      font-size: ${rem('24px')};
      line-height: ${rem('40px')};
      letter-spacing: 0.5px;
      padding: 1rem 2rem 0.4rem;
      color: ${({ theme }) => theme.colors.default};
      text-decoration: none;
      display: block;

      @media (min-width: ${({ theme }) => theme.flexboxgrid.breakpoints.md}rem) {
        line-height: ${rem('26px')};
        font-size: ${rem('18px')};
      }

      &:hover,
      &.active {
        color: ${({ theme }) => theme.colors.primary};
      }
    }
  }
`;

export default withTranslation('common')(Navigation);
